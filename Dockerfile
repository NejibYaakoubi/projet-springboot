# Étape de construction
# Utilisez l'image Maven avec Java 8 pour construire l'application
FROM maven:3.8.4-openjdk-8 AS build

# Définissez le répertoire de travail à /app
WORKDIR /app

# Copiez le contenu du répertoire actuel (contenant le code source) dans le répertoire /app du conteneur
COPY . /app

# Exécutez la commande Maven pour nettoyer et construire l'application
RUN mvn clean install

# Étape d'exécution
# Utilisez l'image OpenJDK avec Java 8 slim pour l'étape d'exécution
FROM openjdk:8-jre-slim

# Définissez à nouveau le répertoire de travail à /app
WORKDIR /app

# Copiez le fichier JAR construit à partir de l'étape de construction dans le répertoire /app du conteneur
COPY --from=build /app/target/*.jar app.jar

# Exposez le port 8080 pour permettre l'accès à l'application
EXPOSE 8080

# Commande à exécuter lors du démarrage du conteneur, lance l'application Java avec le fichier JAR
CMD ["java", "-jar", "app.jar"]
