***Construction et Test avec Docker***

 1. Construisez l'image Docker à partir du projet Spring Boot :

`docker build -t nom_de_votre_image .`

Assurez-vous d'exécuter cette commande dans le répertoire contenant votre fichier Dockerfile.

1.  Exécutez des tests dans le conteneur Docker (facultatif) :


`docker run nom_de_votre_image ./mvnw test`

Cela exécute les tests de votre projet Spring Boot dans le conteneur Docker nouvellement créé. Si vos tests nécessitent une base de données ou d'autres services, assurez-vous que le conteneur est configuré en conséquence.

3. Exécutez le conteneur Docker avec l'application Spring Boot :


`docker run -p 8080:8080 nom_de_votre_image`

Assurez-vous de mapper correctement les ports si votre application utilise un port différent de 8080.

4.  Accédez à votre application :

Ouvrez un navigateur et accédez à http://localhost:8080. Vous devriez voir votre application Spring Boot en cours d'exécution dans le conteneur Docker.

~~N'oubliez pas de personnaliser le texte en remplaçant "nom_de_votre_image" par le nom réel que vous souhaitez donner à votre image Docker. Vous pouvez également ajuster le texte en fonction des détails spécifiques de votre projet et de votre configuration.~~
